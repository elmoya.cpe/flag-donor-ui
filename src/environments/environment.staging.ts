export const environment = {
  production: false,
  apiUrl: window["env"]["apiUrl"] || 'https://rapidpass-api-stage.azurewebsites.net',
  debug: window["env"]["debug"] || false
};
