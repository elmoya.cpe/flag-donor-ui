import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@shared/modules/material/material.module';

import { HomeRoutingModule } from './home-routing.module';

import { HomeComponent } from './home.component';
import { LandingComponent } from './landing/landing.component';

@NgModule({
  declarations: [
    HomeComponent,
    LandingComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
